class AddonsModule {
    get addonIDs() {
        return {
            video: 'youtube_status',
        }
    }

    get #addonsModuleRoot() {
        return 'div.addons__box';
    }

    addonsModuleItem(addonID) {
        return `${this.#addonsModuleRoot} div[addon-id="${addonID}"]`;
    }

    addedAddonsItem(addonID) {
        return `${this.#addonsModuleRoot} div[class="addon--added ${addonID}"]`;
    }
}

export default new AddonsModule;