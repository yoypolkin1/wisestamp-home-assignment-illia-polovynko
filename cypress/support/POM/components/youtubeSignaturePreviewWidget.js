class YouTubeSignaturePreviewWidget {
    get widgetRoot() {
        return 'div.preview-pane';
    }

    videoTitle(videoLink) {
        return `${this.widgetRoot} a[href="${videoLink}"] ~ span`;
    }

    videoPreviewImage(videoLink) {
        return `${this.widgetRoot} a[href="${videoLink}"]`;
    }
}

export default new YouTubeSignaturePreviewWidget;