class EditorTabMenu {
    get tabNames() {
        return {
            details: 'ws-details',
            images: 'ws-images',
            social: 'ws-social',
            template: 'ws-templates',
            design: 'ws-design',
            addons: 'ws-addons',
        }
    };

    get #editorTabMenuRoot() {
        return 'div.editor-tab-menu-placeholder';
    };

    editorTabMenuItem(tabName) {
        return `${this.#editorTabMenuRoot} div.editor-tab-menu-item[tab-name="${tabName}"]`;
    };
}

export default new EditorTabMenu;