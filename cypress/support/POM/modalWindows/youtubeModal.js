class YouTubeModal {
    get styleRadiobuttonTexts() {
        return {
            compact: 'Compact',
            expanded: 'Expanded',
        }
    }

    get modalRoot() {
        return 'div.app__container:has(div.app__title:contains("YouTube video"))';
    }

    get playlistUrlInput() {
        return 'input[name="video_url"]';
    }

    get playlistTitleInput() {
        return 'input[name="user_title"]';
    }

    get addButton() {
        return 'button:contains("Add")';
    }

    get compactRadiobutton() {
        return '#compact-radio-true';
    }

    fontColorsColorpicker(fontColor) {
        return `div.ws-color-picker[ke="${fontColor}"]`;
    }
}

export default new YouTubeModal;
