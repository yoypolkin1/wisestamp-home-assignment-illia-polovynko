import EditorTabMenu from '../support/POM/components/editorTabMenu.js';
import AddonsModule from '../support/POM/modules/addonsModule.js';
import YouTubeModal from '../support/POM/modalWindows/youtubeModal.js';
import YouTubeSignaturePreviewWidget from '../support/POM/components/youtubeSignaturePreviewWidget.js';
import testData from '../fixtures/TC3-test-data.json';
import fontColors from '../fixtures/styleFontColors.json';
import tabNames from '../fixtures/editorTabNames.json';


describe('Verify a user is able to create a signature with YouTube video addon', () => {
    // Pre-condition
    before('The "Apps" module is opened and app is fully loaded', () => {
        cy.visitMainEditorPage();
        cy.get(EditorTabMenu.editorTabMenuItem(tabNames.addons)).click();
        cy.get(EditorTabMenu.editorTabMenuItem(tabNames.addons)).should('have.class', 'editor-tab-menu-item-active');
    });

    before('Modal window "YouTube video" is opened', () => {
        cy.intercept('/api/signatures/render').as('render');
        cy.get(AddonsModule.addonsModuleItem(AddonsModule.addonIDs.video)).click();
        cy.wait('@render');
        cy.get(YouTubeModal.modalRoot).should('be.visible');
    });

    after(() => {
        cy.clearAllCookies();
        cy.clearAllLocalStorage();
        cy.clearAllSessionStorage();
    });

    // Test Steps
    it(`In the text input "YouTube video / playlist URL:" enter the link <${testData.videoLink}>`, () => {
        cy.get(YouTubeModal.playlistUrlInput).type(testData.videoLink);
    });

    it(`In the text input "Video / Playlist title:" enter text <${testData.videoTitle}>`, () => {
        cy.get(YouTubeModal.playlistTitleInput).type(testData.videoTitle);
    });

    it('Select <Compact> radiobutton', () => {
        cy.get(YouTubeModal.compactRadiobutton).click({force: true});
    });

    it('Select <Blue> Font Color', () => {
        cy.get(YouTubeModal.fontColorsColorpicker(fontColors.blue)).click();
    });

    it('Click the "Add" button', () => {
        cy.intercept('/api/signatures/render').as('render');
        cy.get(YouTubeModal.addButton).click();
        cy.wait('@render');
    });

    it('Verify the "Video" addon was added to the left-side panel', () => {
        cy.get(AddonsModule.addedAddonsItem(AddonsModule.addonIDs.video), {timeout: Cypress.config('responseTimeout')}).should('be.visible');
    });

    it(`Verify the "Signature preview widget" contains the YouTube video by following URL: <${testData.videoLink}> by verifying its <href> attribute`, () => {
        cy.get(YouTubeSignaturePreviewWidget.videoPreviewImage(testData.videoLink)).should('be.visible');
    });

    it('Verify the "Signature preview widget" contains the video title <BMW Production in Germany>', () => {
        cy.get(YouTubeSignaturePreviewWidget.videoTitle(testData.videoLink)).should('have.text', 'BMW Production in Germany');
    });

    it('Verify title\'s color is <Blue>', () => {
        cy.get(YouTubeSignaturePreviewWidget.videoTitle(testData.videoLink)).invoke('attr', 'style').then(style => {
            expect(style).to.contain(`color: ${fontColors.blue}`);
        });
    });
});
