import { defineConfig } from "cypress";
import { beforeRunHook, afterRunHook } from "cypress-mochawesome-reporter/lib/index.js";

export default defineConfig({
  pageLoadTimeout: 15000,
  responseTimeout: 20000,
  reporter: "cypress-mochawesome-reporter",
  reporterOptions: {
    charts: true,
    reportPageTitle: 'custom-title',
    embeddedScreenshots: true,
    inlineAssets: true,
    saveAllAttempts: false,
  },
  e2e: {
    baseUrl: "https://webapp.wisestamp.com/",
    viewportHeight: 1080,
    viewportWidth: 1920,
    video: false,
    testIsolation: false,
    setupNodeEvents(on, config) {
      on('before:run', async (details) => {
        console.log('override before:run');
        await beforeRunHook(details);
      });

      on('after:run', async () => {
        console.log('override after:run');
        await afterRunHook();
      });
    },
  },
});
