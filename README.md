# Home Assignment WiseStamp


## Test Cases
**Test Case 1 - Verify the preview widget displays a video banner by hovering the Video button**
Pre-condition: 
1. The "Apps" module is opened and app is fully loaded

Test steps:
1. Verify the "Signature preview widget" displays the default button "Add an app here"
2. Verify the "Enhance your signature" section contains the button "Video"
3. Hover cursor over the "Video" button
4. Verify the "Signature preview widget" displays a video banner after hovering the Video button

**Test Case 2 - Verify clicking the "Video" button opens the "YouTube video" modal window with all necessary fields**
Pre-condition: 
1. The "Apps" module is opened and app is fully loaded

Test Steps:
1. Verify the "Enhance your signature" section contains the button "Video"
2. Click the button "Video"
3. Verify the modal window "YouTube video" is opened.
4. Verify the section  "Enter your video URL & title" contains text input "YouTube video / playlist URL:"
5. Verify the text input "YouTube video / playlist URL:" is enabled
6. Verify the section "Enter your video URL & title" contains text input "Video / Playlist title:"
7. Verify the text input "Video / Playlist title:" is enabled
8. Verify the section "Style" contains radiobuttons "Compact" and "Expanded"
9. Verify the section "Style" contains Font Color colorpicker
10. Verify the section "Style" contains Font Size slider
11. Verify the section "Style" contains alignment options
12. Verify the modal window "YouTube video" contains "Signature preview widget"

**Test Case 3 - Verify a user is able to create a signature with YouTube video addon**
Pre-condition: 
1. The "Apps" module is opened and app is fully loaded
2. Modal window "YouTube video" is opened

Test Steps:
1. In the text input "YouTube video / playlist URL:" enter the link "https://www.youtube.com/watch?v=e3rZp2dUqzQ"
2. In the text input "Video / Playlist title:" enter text "BMW Production in Germany"
3. Select "Compact" radiobutton
4. Select "Blue" Font Color
5. Click the "Add" button
6. Verify the "Signature preview widget" contains the YouTube video by following URL: "https://www.youtube.com/watch?v=e3rZp2dUqzQ" by verifying its "href" attribute
7. Verify the "Signature preview widget" contains the video title "BMW Production in Germany"
8. Verify the "Video" addon was added to the left-side panel
9. Verify title's color is "Blue"

**Test Case 4 - Verify a user is not able to add "Online payments" addon without signing up first**
Pre-condition:
1. The "Apps" module is opened and app is fully loaded

Test Steps:
1. Verify the button "Online payments" is displayed under the section "Call to action"
2. Hover cursor over the button "Online payments"
3. Verify the "Signature preview widget" contains a green button "Make a payment"
4. Verify that after hovering the button "Online payments" a pop-up window appeared with text "This app will be available once you sign up." and button "Signup to use"
5. Click the button "Online payments"
6. Verify that there are no changes in the application state.

**Test Case 5 - Verify added addon saves previously entered data**
Pre-condition:
1. The "Apps" module is opened and app is fully loaded.

Test Steps:
1. Click the "Video" button under the "Enhance your signature" section
2. Verify modal window "YouTube video" is opened
3. In the text input "YouTube video / playlist URL:" enter the link "https://www.youtube.com/watch?v=e3rZp2dUqzQ>
4. In the text input "Video / Playlist title:" enter text "BMW Production in Germany"
5. Select "Compact" radiobutton
6. Select "Blue" Font Color
7. Select "Medium" Font Size
8. Click "Add" button
9. Verify the "Video" addon was added to the addon box panel
10. Click the "Edit" button on the added "Video" addon
11. Verify modal window "YouTube video" is opened
12. Verify the "Signature preview widget" contains the YouTube video by following URL: "https://www.youtube.com/watch?v=e3rZp2dUqzQ" by verifying its "href" attribute
13. Verify the "Signature preview widget" contains the video title "BMW Production in Germany"
14. Verify style radiobutton "Compact" is selected
15. Verify title's color is "Blue"
16. Verify signature's font fize is "Medium"
17. Verify the "left" alignment of text is selected
18. Click the "Cancel" button
19. Verify the modal window "YouTube video" is not displayed anymore
20. Verify the "Video" addon is still attached to the addon box panel

## Getting Started with Automated Test Cases
__The Test Case 3 was automated.__
1. After cloning the repository execute the following command in the terminal:
> npm ci

2. Run the automated test case by executing the following command:
> npx cypress run --browser chrome --headless

Or by executing prepared scripts from the __package.json__ file. 

Using parameter "--headed" allows you to see all the test process with UI interaction.

3. After running the automated test case, the HTML report will be generated. To see the report go to the __cypress/reports/html__ folder. Open the report in the browser.

## Project Structure
1. Under the __cypress__ folder you can find the __e2e__ folder. This folder is used to keep the automated test cases. In this case there is the Test Case 3 described in the begining of this file.
2. The __fixtures__ folder contains needed test data in files with JSON format.
3. The __reports/html__ folder contains reports, that automatically generate after each test run. (FYI: each run the test report will be overwrited!)
4. The __support__ folder contains Page Objects and all the needed files with desribed elements and locators.